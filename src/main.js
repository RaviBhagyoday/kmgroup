import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import vuexStore from './model/state'
import VueRouter from 'vue-router'
import routes from './model/routes'
import mixin from './model/mixin'

Vue.config.productionTip = false

Vue.use(Vuex)
const store = new Vuex.Store(vuexStore)

Vue.use(VueRouter)
const router = new VueRouter(routes)

Vue.mixin(mixin)

setTimeout(() => 
  {
    new Vue({
      render: h => h(App),
      router,
      store,
    }).$mount('#app')
  },
  20 
)
