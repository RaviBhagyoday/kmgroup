export default {
    smtpToken: 'e9572e0f-c131-48e3-8d54-9b60b7fed28b',
    fromEmail: 'kmgroup.viewer@gmail.com',

    /*
    ********************************************************
        main: OBJECT(
            carouselWithBackgroundView: OBJECT(
                content: ARRAY(
                    OBJECT(
                        heading: ARRAY(
                            OBJECT(
                                word: STRING,
                                color: STRING
                            )
                        )
                        description: STRING
                        backgroundImg: OBJECT(
                            1: IMAGE
                            2: IMAGE
                            3: IMAGE
                        )
                    )
                )
            )
            mainCarouselView: OBJECT(
                links: ARRAY(
                    OBJECT(
                        title: STRING
                        cards: ARRAY(
                            OBJECT(
                                img: OBJECT(
                                    1: IMAGE
                                    2: IMAGE
                                    3: IMAGE
                                )
                                name: STRING
                                description: ARRAY(
                                    OBJECT(
                                        word: STRING
                                        color: STRING
                                    )
                                )
                            )
                        )
                    )
                )
                heading: STRING
            )
        )
    ********************************************************
    */
    main: {
        carouselWithBackgroundView: {
            content: [
                {
                    backgroundImg: {
                        1: {
                            webp: require('../assets/webp/SmallCarousel_1/@1x.webp'),
                            jpeg: require('../assets/jpeg/SmallCarousel_1/@1x.jpeg'),
                        },
                        2: {
                            webp: require('../assets/webp/SmallCarousel_1/@2x.webp'),
                            jpeg: require('../assets/jpeg/SmallCarousel_1/@2x.jpeg'),
                        },
                        3: {
                            webp: require('../assets/webp/SmallCarousel_1/@3x.webp'),
                            jpeg: require('../assets/jpeg/SmallCarousel_1/@3x.jpeg'),
                        },
                    },
                    heading: [
                        {
                            word: 'Committed',
                            color: '#494949'
                        },
                        {
                            word: 'To',
                            color: '#494949'
                        },
                        {
                            word: 'Superior',
                            color: '#DF0818'
                        },
                        {
                            word: 'Quality',
                            color: '#DF0818'
                        },
                        {
                            word: 'And',
                            color: '#494949'
                        },
                        {
                            word: 'Services',
                            color: '#494949'
                        },
                    ],
                    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras nibh proin turpis pretium, aliquet libero dolor. At pretium id dictum habitasse nunc. Faucibus a magnis eget in dictumst bibendum vitae ornare. Ullamcorper consectetur sit sit dignissim. Auctor lacus, viverra morbi odio pharetra, eu.'
                },
                {
                    backgroundImg: {
                        1: {
                            webp: require('../assets/webp/SmallCarousel_2/@1x.webp'),
                            jpeg: require('../assets/jpeg/SmallCarousel_2/@1x.jpeg'),
                        },
                        2: {
                            webp: require('../assets/webp/SmallCarousel_2/@2x.webp'),
                            jpeg: require('../assets/jpeg/SmallCarousel_2/@2x.jpeg'),
                        },
                        3: {
                            webp: require('../assets/webp/SmallCarousel_2/@3x.webp'),
                            jpeg: require('../assets/jpeg/SmallCarousel_2/@3x.jpeg'),
                        },
                    },
                    heading: [
                        {
                            word: 'Committed',
                            color: '#DF0818'
                        },
                        {
                            word: 'To',
                            color: '#DF0818'
                        },
                        {
                            word: 'Superior',
                            color: '#494949'
                        },
                        {
                            word: 'Quality',
                            color: '#494949'
                        },
                        {
                            word: 'And',
                            color: '#DF0818'
                        },
                        {
                            word: 'Services',
                            color: '#DF0818'
                        },
                    ],
                    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras nibh proin turpis pretium, aliquet libero dolor. At pretium id dictum habitasse nunc. Faucibus a magnis eget in dictumst bibendum vitae ornare. Ullamcorper consectetur sit sit dignissim. Auctor lacus, viverra morbi odio pharetra, eu.'
                },
                {
                    backgroundImg: {
                        1: {
                            webp: require('../assets/webp/SmallCarousel_3/@1x.webp'),
                            jpeg: require('../assets/jpeg/SmallCarousel_3/@1x.jpeg'),
                        },
                        2: {
                            webp: require('../assets/webp/SmallCarousel_3/@2x.webp'),
                            jpeg: require('../assets/jpeg/SmallCarousel_3/@2x.jpeg'),
                        },
                        3: {
                            webp: require('../assets/webp/SmallCarousel_3/@3x.webp'),
                            jpeg: require('../assets/jpeg/SmallCarousel_3/@3x.jpeg'),
                        },
                    },
                    heading: [
                        {
                            word: 'Committed',
                            color: '#494949'
                        },
                        {
                            word: 'To',
                            color: '#494949'
                        },
                        {
                            word: 'Superior',
                            color: '#DF0818'
                        },
                        {
                            word: 'Quality',
                            color: '#DF0818'
                        },
                        {
                            word: 'And',
                            color: '#494949'
                        },
                        {
                            word: 'Services',
                            color: '#494949'
                        },
                    ],
                    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras nibh proin turpis pretium, aliquet libero dolor. At pretium id dictum habitasse nunc. Faucibus a magnis eget in dictumst bibendum vitae ornare. Ullamcorper consectetur sit sit dignissim. Auctor lacus, viverra morbi odio pharetra, eu.'
                },
            ]
        },
        mainCarouselView: {
            heading: 'Every Thing You Will Ever Need',
            links: [
                {
                    title: 'Industrial',
                    cards: [
                        {
                            img: {
                                1: {
                                    webp: require('../assets/webp/BigCarousel_1/@1x.webp'),
                                    jpeg: require('../assets/jpeg/BigCarousel_1/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/BigCarousel_1/@2x.webp'),
                                    jpeg: require('../assets/jpeg/BigCarousel_1/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/BigCarousel_1/@3x.webp'),
                                    jpeg: require('../assets/jpeg/BigCarousel_1/@3x.jpeg'),
                                },
                            },
                            name: 'Company Name',
                            description: [
                                {
                                    word: 'lorem',
                                    color: '#494949'
                                },
                                {
                                    word: 'Ipsum',
                                    color: '#494949'
                                },
                                {
                                    word: 'something',
                                    color: '#DF0818'
                                },
                                {
                                    word: 'will',
                                    color: '#494949'
                                },
                                {
                                    word: 'surley',
                                    color: '#494949'
                                },
                                {
                                    word: 'replace',
                                    color: '#494949'
                                },
                                {
                                    word: 'this',
                                    color: '#494949'
                                },
                            ]
                        },
                        {
                            img: {
                                1: {
                                    webp: require('../assets/webp/BigCarousel_2/@1x.webp'),
                                    jpeg: require('../assets/jpeg/BigCarousel_2/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/BigCarousel_2/@2x.webp'),
                                    jpeg: require('../assets/jpeg/BigCarousel_2/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/BigCarousel_2/@3x.webp'),
                                    jpeg: require('../assets/jpeg/BigCarousel_2/@3x.jpeg'),
                                },
                            },
                            name: 'Company Name',
                            description: [
                                {
                                    word: 'lorem',
                                    color: '#494949'
                                },
                                {
                                    word: 'Ipsum',
                                    color: '#494949'
                                },
                                {
                                    word: 'something',
                                    color: '#DF0818'
                                },
                                {
                                    word: 'will',
                                    color: '#494949'
                                },
                                {
                                    word: 'surley',
                                    color: '#494949'
                                },
                                {
                                    word: 'replace',
                                    color: '#494949'
                                },
                                {
                                    word: 'this',
                                    color: '#494949'
                                },
                            ]
                        },
                        {
                            img: {
                                1: {
                                    webp: require('../assets/webp/BigCarousel_3/@1x.webp'),
                                    jpeg: require('../assets/jpeg/BigCarousel_3/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/BigCarousel_3/@2x.webp'),
                                    jpeg: require('../assets/jpeg/BigCarousel_3/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/BigCarousel_3/@3x.webp'),
                                    jpeg: require('../assets/jpeg/BigCarousel_3/@3x.jpeg'),
                                },
                            },
                            name: 'Company Name',
                            description: [
                                {
                                    word: 'lorem',
                                    color: '#494949'
                                },
                                {
                                    word: 'Ipsum',
                                    color: '#494949'
                                },
                                {
                                    word: 'something',
                                    color: '#DF0818'
                                },
                                {
                                    word: 'will',
                                    color: '#494949'
                                },
                                {
                                    word: 'surley',
                                    color: '#494949'
                                },
                                {
                                    word: 'replace',
                                    color: '#494949'
                                },
                                {
                                    word: 'this',
                                    color: '#494949'
                                },
                            ]
                        },
                        {
                            img: {
                                1: {
                                    webp: require('../assets/webp/BigCarousel_4/@1x.webp'),
                                    jpeg: require('../assets/jpeg/BigCarousel_4/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/BigCarousel_4/@2x.webp'),
                                    jpeg: require('../assets/jpeg/BigCarousel_4/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/BigCarousel_4/@3x.webp'),
                                    jpeg: require('../assets/jpeg/BigCarousel_4/@3x.jpeg'),
                                },
                            },
                            name: 'Company Name',
                            description: [
                                {
                                    word: 'lorem',
                                    color: '#494949'
                                },
                                {
                                    word: 'Ipsum',
                                    color: '#494949'
                                },
                                {
                                    word: 'something',
                                    color: '#DF0818'
                                },
                                {
                                    word: 'will',
                                    color: '#494949'
                                },
                                {
                                    word: 'surley',
                                    color: '#494949'
                                },
                                {
                                    word: 'replace',
                                    color: '#494949'
                                },
                                {
                                    word: 'this',
                                    color: '#494949'
                                },
                            ]
                        },
                    ]
                },
                {
                    title: 'Pro Services',
                    cards: []
                },
                // {
                //     title: 'Textile',
                //     cards: []
                // },
                // {
                //     title: 'Construction',
                //     cards: []
                // },
                {
                    title: 'IT Services',
                    cards: []
                },
                // {
                //     title: 'Gift and Toys',
                //     cards: []
                // },
                // {
                //     title: 'Advertising',
                //     cards: []
                // },
                // {
                //     title: 'Food Products',
                //     cards: []
                // },
            ]
        },
    },

    /*
    ************************************
        tieups: OBJECT(
            tieupsView: OBJECT(
                heading: STRING
                partnerList: ARRAY(
                    OBJECT(
                        name: STRING,
                        img: OBJECT(
                            1: IMAGE
                            2: IMAGE
                            3: IMAGE
                        )
                    )
                )
            )
        )
    ************************************
    */
    tieups: {
        tieupsView: {
            heading: 'Our Partners',
            partnerList: [
                {
                    name: 'Rovae',
                    img: {
                        1: {
                            webp: require('../assets/webp/Tieups_1/@1x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_1/@1x.jpeg'),
                        },
                        2: {
                            webp: require('../assets/webp/Tieups_1/@2x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_1/@2x.jpeg'),
                        },
                        3: {
                            webp: require('../assets/webp/Tieups_1/@3x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_1/@3x.jpeg'),
                        },
                    }
                },
                {
                    name: 'Rovae',
                    img: {
                        1: {
                            webp: require('../assets/webp/Tieups_2/@1x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_2/@1x.jpeg'),
                        },
                        2: {
                            webp: require('../assets/webp/Tieups_2/@2x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_2/@2x.jpeg'),
                        },
                        3: {
                            webp: require('../assets/webp/Tieups_2/@3x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_2/@3x.jpeg'),
                        },
                    }
                },
                {
                    name: 'Rovae',
                    img: {
                        1: {
                            webp: require('../assets/webp/Tieups_3/@1x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_3/@1x.jpeg'),
                        },
                        2: {
                            webp: require('../assets/webp/Tieups_3/@2x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_3/@2x.jpeg'),
                        },
                        3: {
                            webp: require('../assets/webp/Tieups_3/@3x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_3/@3x.jpeg'),
                        },
                    }
                },
                {
                    name: 'Rovae',
                    img: {
                        1: {
                            webp: require('../assets/webp/Tieups_4/@1x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_4/@1x.jpeg'),
                        },
                        2: {
                            webp: require('../assets/webp/Tieups_4/@2x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_4/@2x.jpeg'),
                        },
                        3: {
                            webp: require('../assets/webp/Tieups_4/@3x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_4/@3x.jpeg'),
                        },
                    }
                },
                {
                    name: 'Rovae',
                    img: {
                        1: {
                            webp: require('../assets/webp/Tieups_5/@1x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_5/@1x.jpeg'),
                        },
                        2: {
                            webp: require('../assets/webp/Tieups_5/@2x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_5/@2x.jpeg'),
                        },
                        3: {
                            webp: require('../assets/webp/Tieups_5/@3x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_5/@3x.jpeg'),
                        },
                    }
                },
                {
                    name: 'Rovae',
                    img: {
                        1: {
                            webp: require('../assets/webp/Tieups_6/@1x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_6/@1x.jpeg'),
                        },
                        2: {
                            webp: require('../assets/webp/Tieups_6/@2x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_6/@2x.jpeg'),
                        },
                        3: {
                            webp: require('../assets/webp/Tieups_6/@3x.webp'),
                            jpeg: require('../assets/jpeg/Tieups_6/@3x.jpeg'),
                        },
                    }
                },
            ]
        },
    },

    /*
    **************************************************************************
        contact: OBJECT(
            contactView: OBJECT(
                image: OBJECT(
                    1: IMAGE
                    2: IMAGE
                    3: IMAGE
                )
                formHeading: STRING
                inputList: ARRAY(
                    OBJECT(
                        id: STRING
                        label: STRING
                        name: STRING
                        type: STRING ['TEXT', 'DROPDOWN', 'TEXTAREA']
                        value: STRING
                        textareaRows
                        dropdownPlaceholder: STRING
                        dropdownOptions: ARRAY(
                            STRING
                        )
                        autocomplete: STRING ['off', '']
                        disable: BOOLEAN
                        error: BOOLEAN
                    )
                )
            )
            questionView: OBJECT(
                heading: STRING,
                subHeading: STRING,
                questionList: ARRAY(
                    OBJECT(
                        question: STRING,
                        answer: STRING
                    )
                )
            )
        )
    **************************************************************************
    */
    contact: {
        contactView: {
            image: {
                1: {
                    webp: require('../assets/webp/Contact/@1x.webp'),
                    jpeg: require('../assets/jpeg/Contact/@1x.jpeg'),
                },
                2: {
                    webp: require('../assets/webp/Contact/@2x.webp'),
                    jpeg: require('../assets/jpeg/Contact/@2x.jpeg'),
                },
                3: {
                    webp: require('../assets/webp/Contact/@3x.webp'),
                    jpeg: require('../assets/jpeg/Contact/@3x.jpeg'),
                },
            },
            formHeading: 'Have doubts?',
            inputList: [
                {
                    id: 'name',
                    label: 'Full Name',
                    name: 'name',
                    type: 'TEXT',
                    value: '',
                    textareaRows: 0,
                    dropdownPlaceholder: '',
                    dropdownOptions: [],
                    autocomplete: 'off',
                    disable: false,
                    error: false,
                },
                {
                    id: 'mobile',
                    label: 'Mobile Number',
                    name: 'mobile',
                    type: 'TEXT',
                    value: '',
                    textareaRows: 0,
                    dropdownPlaceholder: '',
                    dropdownOptions: [],
                    autocomplete: 'off',
                    disable: false,
                    error: false,
                },
                {
                    id: 'email',
                    label: 'Email Id',
                    name: 'email',
                    type: 'TEXT',
                    value: '',
                    textareaRows: 0,
                    dropdownPlaceholder: '',
                    dropdownOptions: [],
                    autocomplete: 'off',
                    disable: false,
                    error: false,
                },
                {
                    id: 'subject',
                    label: 'Subject',
                    name: 'subject',
                    type: 'DROPDOWN',
                    value: '',
                    textareaRows: 0,
                    dropdownPlaceholder: 'Select one',
                    dropdownOptions: [
                        'Option 1',
                        'Option 2',
                        'Option 3'
                    ],
                    autocomplete: 'off',
                    disable: false,
                    error: false,
                },
                {
                    id: 'message',
                    label: 'Message',
                    name: 'message',
                    type: 'TEXTAREA',
                    value: '',
                    textareaRows: 4,
                    dropdownPlaceholder: '',
                    dropdownOptions: [],
                    autocomplete: 'off',
                    disable: false,
                    error: false,
                },
            ]
        },
        questionView: {
            heading: 'Got Questions?',
            subHeading: 'Perfect, we\'ve got answers',
            questionList: [
                {
                    question: 'How do I apply?',
                    answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor lorem sociis in ipsum leo semper gravida nunc dictum. Habitasse tincidunt ut congue euismod facilisis. Volutpat id lectus sed dui, curabitur ante euismod porta ipsum. Arcu, amet sit odio suscipit suspendisse.'
                },
                {
                    question: 'Why can\'t I sign in?',
                    answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor lorem sociis in ipsum leo semper gravida nunc dictum. Habitasse tincidunt ut congue euismod facilisis. Volutpat id lectus sed dui, curabitur ante euismod porta ipsum. Arcu, amet sit odio suscipit suspendisse.'
                },
                {
                    question: 'What are entry requirements?',
                    answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor lorem sociis in ipsum leo semper gravida nunc dictum. Habitasse tincidunt ut congue euismod facilisis. Volutpat id lectus sed dui, curabitur ante euismod porta ipsum. Arcu, amet sit odio suscipit suspendisse.'
                },
                {
                    question: 'What are UCAS Tariff points?',
                    answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor lorem sociis in ipsum leo semper gravida nunc dictum. Habitasse tincidunt ut congue euismod facilisis. Volutpat id lectus sed dui, curabitur ante euismod porta ipsum. Arcu, amet sit odio suscipit suspendisse.'
                },
                {
                    question: 'Where can I fing out about funding?',
                    answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor lorem sociis in ipsum leo semper gravida nunc dictum. Habitasse tincidunt ut congue euismod facilisis. Volutpat id lectus sed dui, curabitur ante euismod porta ipsum. Arcu, amet sit odio suscipit suspendisse.'
                },
                {
                    question: 'What should I include in my personal statement?',
                    answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor lorem sociis in ipsum leo semper gravida nunc dictum. Habitasse tincidunt ut congue euismod facilisis. Volutpat id lectus sed dui, curabitur ante euismod porta ipsum. Arcu, amet sit odio suscipit suspendisse.'
                },
                {
                    question: 'How do I get a reference?',
                    answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor lorem sociis in ipsum leo semper gravida nunc dictum. Habitasse tincidunt ut congue euismod facilisis. Volutpat id lectus sed dui, curabitur ante euismod porta ipsum. Arcu, amet sit odio suscipit suspendisse.'
                },
            ]
        },
    },

    /*
        *****************************************************************************
        enquiry: OBJECT(
            enquiryView: OBJECT(
                backgroundImage: OBJECT(
                    1: IMAGE
                    2: IMAGE
                    3: IMAGE
                )
                formHeading: STRING
                inputList: ARRAY(
                    OBJECT(
                        id: STRING
                        label: STRING
                        name: STRING
                        type: STRING ['TEXT', 'DROPDOWN', 'TEXTAREA']
                        value: STRING
                        dropdownPlaceholder: STRING
                        dropdownOptions: ARRAY(
                            STRING
                        )
                        autocomplete: STRING ['off', '']
                        disable: BOOLEAN
                        error: BOOLEAN
                    )
                )
            )
            enquiryTextView: OBJECT(
                heading: STRING
                paragraph: STRING
            )
        )
    *****************************************************************************
    */
    enquiry: {
        enquiryView: {
            backgroundImage: {
                1: {
                    webp: require('../assets/webp/Enquiry/@1x.webp'),
                    jpeg: require('../assets/jpeg/Enquiry/@1x.jpeg'),
                },
                2: {
                    webp: require('../assets/webp/Enquiry/@2x.webp'),
                    jpeg: require('../assets/jpeg/Enquiry/@2x.jpeg'),
                },
                3: {
                    webp: require('../assets/webp/Enquiry/@3x.webp'),
                    jpeg: require('../assets/jpeg/Enquiry/@3x.jpeg'),
                },
            },
            formHeading: 'Want Something?',
            inputList: [
                {
                    id: 'name',
                    label: 'Full Name',
                    name: 'name',
                    type: 'TEXT',
                    value: '',
                    textareaRows: 0,
                    dropdownPlaceholder: '',
                    dropdownOptions: [],
                    autocomplete: 'off',
                    disable: false,
                    error: false,
                },
                {
                    id: 'mobile',
                    label: 'Mobile Number',
                    name: 'mobile',
                    type: 'TEXT',
                    value: '',
                    textareaRows: 0,
                    dropdownPlaceholder: '',
                    dropdownOptions: [],
                    autocomplete: 'off',
                    disable: false,
                    error: false,
                },
                {
                    id: 'email',
                    label: 'Email Id',
                    name: 'email',
                    type: 'TEXT',
                    value: '',
                    textareaRows: 0,
                    dropdownPlaceholder: '',
                    dropdownOptions: [],
                    autocomplete: 'off',
                    disable: false,
                    error: false,
                },
                {
                    id: 'subject',
                    label: 'Subject',
                    name: 'subject',
                    type: 'DROPDOWN',
                    value: '',
                    textareaRows: 0,
                    dropdownPlaceholder: 'Select One',
                    dropdownOptions: [
                        'Option 1',
                        'Option 2',
                        'Option 3'
                    ],
                    autocomplete: 'off',
                    disable: false,
                    error: false,
                },
                {
                    id: 'requirements',
                    label: 'Requirements',
                    name: 'requirements',
                    type: 'TEXTAREA',
                    value: '',
                    textareaRows: 6,
                    dropdownPlaceholder: '',
                    dropdownOptions: [],
                    autocomplete: 'off',
                    disable: false,
                    error: false,
                },
                {
                    id: 'address',
                    label: 'Address',
                    name: 'address',
                    type: 'TEXTAREA',
                    value: '',
                    textareaRows: 6,
                    dropdownPlaceholder: '',
                    dropdownOptions: [],
                    autocomplete: 'off',
                    disable: false,
                    error: false,
                },
            ]
        },
        enquiryTextView: {
            heading: 'Lorem ipsum',
            paragraph: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dictumst urna, pulvinar vel platea faucibus commodo turpis. Nec, adipiscing neque in nunc, mattis egestas auctor tortor. Enim egestas magnis habitasse augue facilisi ultricies auctor nunc. Ante sed sed lectus nibh iaculis arcu. Nunc neque commodo metus viverra eget euismod id lectus. Velit dis sit consectetur vulputate in aliquam dignissim tellus nibh. Vitae, et magnis odio fringilla morbi eget auctor quis sit. Quis mattis sed proin proin dui facilisis aliquam adipiscing nunc. Duis etiam magna suspendisse consequat. Elementum fames tempor, metus condimentum et mauris natoque rhoncus. Tempor elit odio leo id. Convallis nunc, sollicitudin id in nunc vitae nulla interdum in. Non ullamcorper viverra accumsan cursus. Semper metus mollis ac malesuada eu. Nec commodo eget nunc leo iaculis. Urna pellentesque sem ut non nullam. Id urna ornare eget neque integer porttitor dui id sodales. Vulputate proin nulla nisl etiam pulvinar venenatis, tellus est sed. Rhoncus aenean pharetra fames vitae, nisi, cras dolor sit. Sed eu lacinia condimentum hendrerit. Ultrices lacus amet, suscipit lacus, varius purus placerat. Tincidunt nisl dolor dignissim fermentum tincidunt. Nisi, quis sed nulla consectetur phasellus.',
        },
    },


    /*
    ********************************************
        navbar: OBJECT(
            navbarView: OBJECT(
                navList: ARRAY(
                    OBJECT(
                        title: STRING
                        children: navList
                        route: ROUTE
                    )
                )
                navException: OBJECT(
                    title: STRING,
                    route: ROUTE
                )
            )
        )
    ********************************************
    */
    navbar: {
        navbarView: {
            navList: [
                // {
                //     title: 'Tieups',
                //     route: {
                //         name: 'Tieups',
                //     },
                //     children: [],
                // },
                {
                    title: 'Services',
                    route: null,
                    children: [
                        {
                            title: 'Industrial Products',
                            route: {
                                name: 'Industrial Products'
                            },
                            children: []
                        },
                        {
                            title: 'Pro Services',
                            route: null,
                            children: []
                        },
                        // {
                        //     title: 'Construction',
                        //     route: {
                        //         name: 'Construction Services'
                        //     },
                        //     children: []
                        // },
                        {
                            title: 'IT Services',
                            route: {
                                name: 'IT Services'
                            },
                            children: []
                        },
                        // {
                        //     title: 'Gift and Toys',
                        //     route: null,
                        //     children: []
                        // },
                        // {
                        //     title: 'Advertising',
                        //     route: null,
                        //     children: []
                        // },
                        // {
                        //     title: 'Food Products',
                        //     route: null,
                        //     children: []
                        // },
                        // {
                        //     title: 'Textile',
                        //     route: null,
                        //     children: []
                        // },
                    ]
                },
                {
                    title: 'Contact',
                    route: {
                        name: 'Contact',
                    },
                    children: []
                },
                {
                    title: 'Enquiry',
                    route: {
                        name: 'Enquiry',
                    },
                    children: []
                },
            ],
            navException: {
                // title: 'JoinUs',
                // route: null,
            }
        },
    },

    /*
    ***********************************************
        productOrService: OBJECT(
            <item-number>: OBJECT(
                productOrServiceView: OBJECT(
                    productImg: OBJECT(
                        1: IMAGE,
                        2: IMAGE,
                        3: IMAGE
                    )
                    serviceImg: OBJECT(
                        1: IMAGE,
                        2: IMAGE,
                        3: IMAGE
                    )
                    heading: STRING
                    productName: STRING
                    serviceName: STRING
                    productRoute: ROUTE
                    serviceRoute: ROUTE
                )
            )
        )
    ************************************************
    */
    productOrService: {
        1: {
            productOrServiceView: {
                productImg: {
                    1: {
                        webp: require('../assets/webp/ProductOrService_1/@1x.webp'),
                        jpeg: require('../assets/jpeg/ProductOrService_1/@1x.jpeg'),
                    },
                    2: {
                        webp: require('../assets/webp/ProductOrService_1/@2x.webp'),
                        jpeg: require('../assets/jpeg/ProductOrService_1/@2x.jpeg'),
                    },
                    3: {
                        webp: require('../assets/webp/ProductOrService_1/@3x.webp'),
                        jpeg: require('../assets/jpeg/ProductOrService_1/@3x.jpeg'),
                    },
                },
                serviceImg: {
                    1: {
                        webp: require('../assets/webp/ProductOrService_2/@1x.webp'),
                        jpeg: require('../assets/jpeg/ProductOrService_2/@1x.jpeg'),
                    },
                    2: {
                        webp: require('../assets/webp/ProductOrService_2/@2x.webp'),
                        jpeg: require('../assets/jpeg/ProductOrService_2/@2x.jpeg'),
                    },
                    3: {
                        webp: require('../assets/webp/ProductOrService_2/@3x.webp'),
                        jpeg: require('../assets/jpeg/ProductOrService_2/@3x.jpeg'),
                    },
                },
                heading: 'Construction',
                productName: 'Products',
                serviceName: 'Services',
                productRoute: {
                    name: 'Construction Services / Oil and Gas Products'
                },
                serviceRoute: {
                    name: 'Construction Services / IT Services'
                }
            },
        }
    },

    /*
    ******************************************************
        productList: OBJECT(
            <item-number>: OBJECT(
                productCardView: OBJECT(
                    heading: STRING
                    cardList: ARRAY(
                        OBJECT(
                            image: OBJECT(
                                1: OBJECT(
                                    webp: IMAGE
                                    jpeg: IMAGE
                                )
                                2: OBJECT(
                                    webp: IMAGE
                                    jpeg: IMAGE
                                )
                                3: OBJECT(
                                    webp: IMAGE
                                    jpeg: IMAGE
                                )
                            )
                            title: STRING,
                            subTitle: STRING,
                            code: STRING
                            infoList: OBJECT(
                                left: ARRAY(
                                    OBJECT(
                                        title: STRING
                                        value: STRING
                                    )
                                )
                                right: ARRAY(
                                    OBJECT(
                                        title: STRING
                                        value: STRING
                                    )
                                )
                                bottom: OBJECT(
                                    title: STRING
                                    value: STRING
                                )
                            )
                            route: ROUTE
                        )
                    )
                )
            )
        )
    **********************************************************
    */
    productList: {
        1: {
            productCardView: {
                heading: 'Industrial Products',
                cardList: [
                    {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_UPS/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_UPS/@3x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_UPS/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_UPS/@3x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_UPS/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_UPS/@3x.jpeg'),
                            },
                        },
                        title: 'UPS',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: '',
                                    value: ''
                                },
                                {
                                    title: '',
                                    value: ''
                                },
                                {
                                    title: '',
                                    value: ''
                                },
                            ],
                            right: [
                                {
                                    title: '',
                                    value: ''
                                },
                                {
                                    title: '',
                                    value: ''
                                },
                                {
                                    title: '',
                                    value: ''
                                },
                            ],
                            bottom: {
                                title: '',
                                value: ''
                            }
                        },
                        route: {
                            name: 'Industrial Products / UPS',
                        },
                    },
                    {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Sleeving/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Sleeving/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Sleeving/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Sleeving/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Sleeving/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Sleeving/@3x.jpeg'),
                            },
                        },
                        title: 'Sleeving',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: '',
                                    value: ''
                                },
                                {
                                    title: '',
                                    value: ''
                                },
                                {
                                    title: '',
                                    value: ''
                                },
                            ],
                            right: [
                                {
                                    title: '',
                                    value: ''
                                },
                                {
                                    title: '',
                                    value: ''
                                },
                                {
                                    title: '',
                                    value: ''
                                },
                            ],
                            bottom: {
                                title: '',
                                value: ''
                            }
                        },
                        route: {
                            name: 'Industrial Products / Sleeving',
                        }
                    },
                    {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Valves/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Valves/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Valves/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Valves/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Valves/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Valves/@3x.jpeg'),
                            },
                        },
                        title: 'Valves',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Valves',
                        }
                    },
                    {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Transformers/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Transformers/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Transformers/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Transformers/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Transformers/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Transformers/@3x.jpeg'),
                            },
                        },
                        title: 'Transformers',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Transformers',
                        }
                    },
                    {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Sensor/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Sensor/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Sensor/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Sensor/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Sensor/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Sensor/@3x.jpeg'),
                            },
                        },
                        title: 'Encoders & Sensors',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Encoders & Sensors',
                        }
                    },
                    {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Relay/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Relay/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Relay/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Relay/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Relay/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Relay/@3x.jpeg'),
                            },
                        },
                        title: 'Relays',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Relays',
                        }
                    },
                    {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Pumps/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Pumps/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Pumps/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Pumps/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Pumps/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Pumps/@3x.jpeg'),
                            },
                        },
                        title: 'Pumps',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Pumps',
                        }
                    },
                    {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Motor/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Motor/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Motor/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Motor/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Motor/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Motor/@3x.jpeg'),
                            },
                        },
                        title: 'Motors',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Motors',
                        }
                    },
                    {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_PandT/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_PandT/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_PandT/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_PandT/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_PandT/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_PandT/@3x.jpeg'),
                            },
                        },
                        title: 'Pressure & Temprature',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Pressure & Temperature',
                        }
                    },{
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Tester/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Tester/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Tester/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Tester/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Tester/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Tester/@3x.jpeg'),
                            },
                        },
                        title: 'Testers',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Testers',
                        }
                    },
                    {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Cables/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Cables/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Cables/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Cables/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Cables/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Cables/@3x.jpeg'),
                            },
                        },
                        title: 'Cables',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Cables',
                        }
                    },{
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Electronics/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Electronics/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Electronics/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Electronics/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Electronics/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Electronics/@3x.jpeg'),
                            },
                        },
                        title: 'Electronics',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Electronics',
                        }
                    },
                    {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Instruments/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Instruments/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Instruments/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Instruments/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Instruments/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Instruments/@3x.jpeg'),
                            },
                        },
                        title: 'Instruments',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Instruments',
                        }
                    },
                    {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Bearing/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Bearing/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Bearing/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Bearing/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Bearing/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Bearing/@3x.jpeg'),
                            },
                        },
                        title: 'Bearing',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Bearing',
                        }
                    }, {
                        image: {
                            1: {
                                webp: require('../assets/webp/Industrial_Battery/@1x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Battery/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/Industrial_Battery/@2x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Battery/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/Industrial_Battery/@3x.webp'),
                                jpeg: require('../assets/jpeg/Industrial_Battery/@3x.jpeg'),
                            },
                        },
                        title: 'Battery',
                        subTitle: 'extra info like company name',
                        code: 'AED 59',
                        infoList: {
                            left: [
                                {
                                    title: 'Width',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            right: [
                                {
                                    title: 'Height',
                                    value: '3000mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                                {
                                    title: 'Something',
                                    value: '200mm'
                                },
                            ],
                            bottom: {
                                title: 'Something',
                                value: 'We can also wright something something something something over here.'
                            }
                        },
                        route: {
                            name: 'Industrial Products / Battery',
                        }
                    },
                ]
            }
        },
    },

    /*
    ***********************************************
        product: OBJECT(
            <item-number>: OBJECT(
                productInfoView: OBJECT(
                    productList: ARRAY(
                        imageList: ARRAY(
                            OBJECT(
                                1: OBJECT(
                                    webp: IMAGE
                                    jpeg: IMAGE
                                )
                                2: OBJECT(
                                    webp: IMAGE
                                    jpeg: IMAGE
                                )
                                3: OBJECT(
                                    webp: IMAGE
                                    jpeg: IMAGE
                                )
                            )
                        )
                        name: STRING
                        heading: STRING
                        code: STRING
                        dimension: STRING
                        description: STRING
                    )
                )
            )
        )
    ***********************************************
    */
    product: {
        1: {
            productInfoView: {
                productList: [
                    {
                        name: 'UPS',
                        heading: 'Bulk Purchase available',
                        code: '',
                        dimension: '',
                        description: 'UPS Single phase/Three-phase, UPS kva three-phase Rack UPS, Software & accessories, Stabilizers, Frequency Converters, Static Transfer Switch, Inverter ',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_UPS/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_UPS/@3x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_UPS/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_UPS/@3x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_UPS/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_UPS/@3x.jpeg'),
                                },
                            },
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_UPS_1/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_UPS_1/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_UPS_1/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_UPS_1/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_UPS_1/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_UPS_1/@3x.jpeg'),
                                },
                            },
                            // {
                            //     1: {
                            //         webp: require('../assets/webp/ProductInfo_Small_2/@1x.webp'),
                            //         jpeg: require('../assets/jpeg/ProductInfo_Small_2/@1x.jpeg'),
                            //     },
                            //     2: {
                            //         webp: require('../assets/webp/ProductInfo_Small_2/@2x.webp'),
                            //         jpeg: require('../assets/jpeg/ProductInfo_Small_2/@2x.jpeg'),
                            //     },
                            //     3: {
                            //         webp: require('../assets/webp/ProductInfo_Small_2/@3x.webp'),
                            //         jpeg: require('../assets/jpeg/ProductInfo_Small_2/@3x.jpeg'),
                            //     },
                            // },
                            // {
                            //     1: {
                            //         webp: require('../assets/webp/ProductInfo_Small_3/@1x.webp'),
                            //         jpeg: require('../assets/jpeg/ProductInfo_Small_3/@1x.jpeg'),
                            //     },
                            //     2: {
                            //         webp: require('../assets/webp/ProductInfo_Small_3/@2x.webp'),
                            //         jpeg: require('../assets/jpeg/ProductInfo_Small_3/@2x.jpeg'),
                            //     },
                            //     3: {
                            //         webp: require('../assets/webp/ProductInfo_Small_3/@3x.webp'),
                            //         jpeg: require('../assets/jpeg/ProductInfo_Small_3/@3x.jpeg'),
                            //     },
                            // },
                        ],
                    },
                    {
                        name: 'Sleeving',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Flexible Electrical and Thermal Insulating Sleevings',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Sleeving/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Sleeving/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Sleeving/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Sleeving/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Sleeving/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Sleeving/@3x.jpeg'),
                                },
                            },
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Sleeving_1/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Sleeving_1/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Sleeving_1/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Sleeving_1/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Sleeving_1/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Sleeving_1/@3x.jpeg'),
                                },
                            },
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Sleeving_2/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Sleeving_2/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Sleeving_2/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Sleeving_2/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Sleeving_2/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Sleeving_2/@3x.jpeg'),
                                },
                            },

                        ]
                    },
                    {
                        name: 'Valves',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Globe single & double seated valve, globe angle, Teflon, globe 3 way, flush bottom valve, butterfly valve, O Ball Valve, V Ball Valve, Control valve & Misc.',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Valves/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Valves/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Valves/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Valves/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Valves/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Valves/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                    {
                        name: 'Transformers',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Control Transformer, Safety Isolation Transformers, PCB Type Transformers, Three Phase Transformers, Medical Transformers, Detuned Filter Reactors, Shunt Reactors, And High Freq Ferrite Core Transformers.',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Transformers/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Transformers/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Transformers/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Transformers/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Transformers/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Transformers/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                    {
                        name: 'Encoders & Sensors',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Inductive Sensors, Capacitive sensors, Magnetic Field Sensors, Ultrasonic sensors, Photoelectric Sensors, Vision Sensors, Door, Gates & Elevators Sensors, Safety Sensors & Controls System, Rotary Encoders, Logic control units and accessories.',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Sensor/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Sensor/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Sensor/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Sensor/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Sensor/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Sensor/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                    {
                        name: 'Relays',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Phase Failure Relays, Voltage/Current/Frequency/Power monitoring Relays, Motor/Pump Protection Relays, Electronic Timers, Liquid Level controllers, Automation product, Power Line Transducers, PBC relays, Industrial relays.',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Relay/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Relay/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Relay/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Relay/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Relay/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Relay/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                    {
                        name: 'Pumps',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Process Pumps, Magnetic Driven Process Pumps, Hot Fluid & General Service Pumps, Liquid Ring Vacuum Pumps, Positive Displacement Blowers, Hose Pump.',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Pumps/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Pumps/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Pumps/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Pumps/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Pumps/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Pumps/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                    {
                        name: 'Motors',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Slip Ring Motors. Marine Motors, Explosion Proof Motors and other Electric Motors as per request.',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Motor/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Motor/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Motor/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Motor/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Motor/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Motor/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                    {
                        name: 'Pressure & Temperature',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Pressure Gauges and Thermometers, Pressure Switches & temperature switches ,Thermocouples and thermo resistances',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_PandT/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_PandT/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_PandT/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_PandT/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_PandT/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_PandT/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                    {
                        name: 'Testers',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Digital Multi meters Clamp on meters, Auxiliary Meters, Installation testers & Power analyzers, Battery Impedance Tester.',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Tester/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Tester/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Tester/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Tester/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Tester/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Tester/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                    {
                        name: 'Cables',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Building wires, Power cables, Medium voltage power cables, High voltage power cables, Low smoke and fume fire retardant cables, Marine and offshore cables, Railway cables, copper telecommunication cables & Fiber optic cables.',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Cables/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Cables/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Cables/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Cables/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Cables/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Cables/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                    {
                        name: 'Electronics',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Rectifier Battery Charger, DC/DC Converters, DC/AC Inverter, Energy Stations, Battery Monitoring Systems, Fault Recorders',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Electronics/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Electronics/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Electronics/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Electronics/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Electronics/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Electronics/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                    {
                        name: 'Instruments',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Flexible Electrical and Thermal Insulating Sleevings',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Instruments/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Instruments/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Instruments/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Instruments/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Instruments/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Instruments/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                    {
                        name: 'Bearing',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: 'Cyclic Panel, Digital Time Switch, Single phase / Three Phase Starter, Phase failure relays, Timers / Temp Controller & Counters, Wires & Cables & Water level Controller',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Bearing/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Bearing/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Bearing/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Bearing/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Bearing/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Bearing/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                    {
                        name: 'Battery',
                        heading: 'Bulk Purchase Available',
                        code: '',
                        dimension: '',
                        description: '** Dynasty – VRLA Ranging from 26-200Ah ** TEL – VRLA Ranging from 33-200Ah ** msEndur II - VRLA Ranging from 330-2500Ah ** XT – Vented Lead Calcium Ranging from 330-2500Ah ** LCT - Vented Lead Calcium Ranging from 630-2300Ah ** CPV - Vented Solar Ranging from 300-2300Ah ** MCTII - Vented Lead Calcium Ranging from 2000-3000Ah',
                        imageList: [
                            {
                                1: {
                                    webp: require('../assets/webp/Industrial_Battery/@1x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Battery/@1x.jpeg'),
                                },
                                2: {
                                    webp: require('../assets/webp/Industrial_Battery/@2x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Battery/@2x.jpeg'),
                                },
                                3: {
                                    webp: require('../assets/webp/Industrial_Battery/@3x.webp'),
                                    jpeg: require('../assets/jpeg/Industrial_Battery/@3x.jpeg'),
                                },
                            },
                        ]
                    },
                ]
            }
        },

    },

    /*
    *****************************************************
        serviceList: OBJECT(
            <item-number>: OBJECT(
                serviceImageView: OBJECT(
                    backgroundImage: OBJECT(
                        1: IMAGE
                        2: IMAGE
                        3: IMAGE
                    )
                    heading: STRING
                    description: STRING
                    route: ROUTE
                )
                serviceCardView: OBJECT(
                    heading: STRING,
                    cardList: ARRAY(
                        OBJECT(
                            title: STRING
                            description: STRING
                            backgroundImage: OBJECT(
                                1: IMAGE
                                2: IMAGE
                                3: IMAGE
                            )
                        )
                    )
                    gridColumnCount: INTEGER
                )
            )
        )
    *****************************************************
    */
    serviceList: {
        1: {
            serviceImageView: {
                backgroundImage: {
                    1: {
                        webp: require('../assets/webp/ServiceImage/@1x.webp'),
                        jpeg: require('../assets/jpeg/ServiceImage/@1x.jpeg'),
                    },
                    2: {
                        webp: require('../assets/webp/ServiceImage/@2x.webp'),
                        jpeg: require('../assets/jpeg/ServiceImage/@2x.jpeg'),
                    },
                    3: {
                        webp: require('../assets/webp/ServiceImage/@3x.webp'),
                        jpeg: require('../assets/jpeg/ServiceImage/@3x.jpeg'),
                    },
                },
                heading: 'IT Services',
                description: 'Rovae is an all-in-one digital platform for small businesses based in India, founded by a group of IT professionals. We are here to empower small businesses around the world to start and grow with our digital expertise. We would love to learn, collaborate, and create. We also specialize in providing services such as Web Development, Cloud Services, IT Security, Marketing, IT Consultancy, and much more. We are flexible to change according to your needs.',
                route: {
                    name: 'Contact'
                }
            },
            serviceCardView: {
                heading: 'What We Offer',
                cardList: [
                    {
                        route: {
                            name: 'IT Services / Cloud Solutions'
                        },
                        title: 'Cloud Solutions',
                        backgroundImage:  {
                            1: {
                                webp: require('../assets/webp/ServiceCard/@1x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/ServiceCard/@2x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/ServiceCard/@3x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@3x.jpeg'),
                            },
                        },
                        description: 'Setting up and maintaining infrastructure to handle scale, reliability and performance of application for in a cost effective way.'
                    },
                    {
                        route: {
                            name: 'IT Services / Web Development'
                        },
                        title: 'Web Development',
                        backgroundImage: {
                            1: {
                                webp: require('../assets/webp/ServiceCard/@1x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/ServiceCard/@2x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/ServiceCard/@3x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@3x.jpeg'),
                            },
                        },
                        description: 'Our team has a long track record of engineering and maintaining scalable web applications; especially Software-as-a-Service (SaaS) applications.'
                    },
                    {
                        route: {
                            name: 'IT Services / IT Consultancy'
                        },
                        title: 'IT Consultancy',
                        backgroundImage: {
                            1: {
                                webp: require('../assets/webp/ServiceCard/@1x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/ServiceCard/@2x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/ServiceCard/@3x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@3x.jpeg'),
                            },
                        },
                        description: 'We have passion for problem solving. We help startups and businesses move faster, and be more effective by identifying right tools and architecture for their software.'
                    },
                    {
                        route: {
                            name: 'IT Services / UI//UX Design'
                        },
                        title: 'UI/UX Design',
                        backgroundImage: {
                            1: {
                                webp: require('../assets/webp/ServiceCard/@1x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/ServiceCard/@2x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/ServiceCard/@3x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@3x.jpeg'),
                            },
                        },
                        description: 'We design and develop the most responsive web that you can ever think of. Moreover, we try to make our website in such a way that it response to all types of smart devices irrespective of their sizes'
                    },
                    {
                        route: {
                            name: 'IT Services / Digital Marketing'
                        },
                        title: 'Digital Marketing',
                        backgroundImage: {
                            1: {
                                webp: require('../assets/webp/ServiceCard/@1x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/ServiceCard/@2x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/ServiceCard/@3x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@3x.jpeg'),
                            },
                        },
                        description: 'The power of Internet and its influence on businesses worldwide cannot be ignored. No business, big or small, national or local, can afford to stay away from the online world'
                    },
                    {
                        route: {
                            name: 'IT Services / Mobile Development'
                        },
                        title: 'Mobile Development',
                        backgroundImage: {
                            1: {
                                webp: require('../assets/webp/ServiceCard/@1x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@1x.jpeg'),
                            },
                            2: {
                                webp: require('../assets/webp/ServiceCard/@2x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@2x.jpeg'),
                            },
                            3: {
                                webp: require('../assets/webp/ServiceCard/@3x.webp'),
                                jpeg: require('../assets/jpeg/ServiceCard/@3x.jpeg'),
                            },
                        },
                        description: 'With increasing dependence on mobile devices, mobile applications have seen a tremendous boost. From games to shopping, bill payment to social networking, there are apps for almost everything'
                    },
                ],
                gridColumnCount: 4
            },
        }
    },

    /*
    **************************************************
        service: OBJECT(
            <item-number>: OBJECT(
                serviceContentView: OBJECT(
                    name: STRING,
                    heading: STRING,
                    subHeading: STRING,
                    description: STRING,
                    backgroundImage: OBJECT(
                        1: IMAGE
                        2: IMAGE
                        3: IMAGE
                    )
                )
                questionView: OBJECT(
                    exists: BOOLEAN
                    heading: STRING,
                    subHeading: STRING,
                    questionList: ARRAY(
                        OBJECT(
                            question: STRING,
                            answer: STRING
                        )
                    )
                )
            )
        )
    **************************************************
    */
    service: {
        1: {
            serviceContentView: {
                backgroundImage: {
                    1: {
                        webp: require('../assets/webp/ServiceInfo/@1x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@1x.jpeg'),
                    },
                    2: {
                        webp: require('../assets/webp/ServiceInfo/@2x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@2x.jpeg'),
                    },
                    3: {
                        webp: require('../assets/webp/ServiceInfo/@3x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@3x.jpeg'),
                    },
                },
                name: 'Cloud Solutions',
                heading: 'Cloud Solution',
                subHeading: '',
                description: 'Elevate your business by using abundant power of cloud platforms. The cloud platforms have brought radical changes in the IT industry. It changed the approach of accessing information, business applications and comprehensive IT infrastructure. By hiring the best cloud infrastructure service provider company you can actually yield complete potential of the cloud platform because there are many hidden gems in the cloud platform that can be uncovered by the experts only. Whether you are taking a leap towards the cloud platforms from your legacy system or you are already using one of the best cloud based infrastructure, we can assure that you will be impressed with our expertise and knowledge. We follow a strategic working model to help our clients in the process of identifying the best cloud platform for their business and optimizing their business applications and processes along with other cloud infrastructure services that can benefit them to yield maximum benefits of the amazing nascent cloud platforms.',
            },
            questionView: {
                exists: false,
                heading: '',
                subHeading: '',
                questionList: []
            }
        },
        2: {
            serviceContentView: {
                backgroundImage: {
                    1: {
                        webp: require('../assets/webp/ServiceInfo/@1x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@1x.jpeg'),
                    },
                    2: {
                        webp: require('../assets/webp/ServiceInfo/@2x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@2x.jpeg'),
                    },
                    3: {
                        webp: require('../assets/webp/ServiceInfo/@3x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@3x.jpeg'),
                    },
                },
                name: 'Web Development',
                heading: 'Web Development',
                subHeading: '',
                description: 'We provide top-notch services in the field of software development. Software development is a difficult task, which requires skills and technical expertise. We are proud of our highly skilled development team that delivers quality results time and again. Our motto is to cater our clients with the best possible software solution that is scalable and reliable. We use cloud solutions and latest technologies to accelerate your business to the next level. Providing customer-centric and user-friendly software that is fast, efficient and attractive is the USP of our company. We build both, user friendly front-end and robust back-end for software. Also, we can assist you to transform your legacy system into the modern software. We are good at the technologies and tools, but what makes us stand out is a knack for solving the problems in the right way with the right tools.',
            },
            questionView: {
                exists: false,
                heading: '',
                subHeading: '',
                questionList: []
            }
        },
        3: {
            serviceContentView: {
                backgroundImage: {
                    1: {
                        webp: require('../assets/webp/ServiceInfo/@1x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@1x.jpeg'),
                    },
                    2: {
                        webp: require('../assets/webp/ServiceInfo/@2x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@2x.jpeg'),
                    },
                    3: {
                        webp: require('../assets/webp/ServiceInfo/@3x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@3x.jpeg'),
                    },
                },
                name: 'IT Consultancy',
                heading: 'IT Consultancy',
                subHeading: '',
                description: 'Day by day technologies are taking leaps and bounds. You have to match pace with the revolutionary innovations and dynamic changes in the IT landscape. The technological inventions create both, opportunities and threat and we are here at your assistance to help you capture the opportunities and removing the threats by earning a competitive edge with our technology consulting services. Over the past few years, we succeed in benefiting our clients with our technological expertise and these years of experience have made us one of the best technology consultants. We help our clients to think big by modernizing their software and IT infrastructure to leverage a plethora of benefits it brings. We help them embrace the new and emerging technologies and implement them in their business seamlessly.',
            },
            questionView: {
                exists: false,
                heading: '',
                subHeading: '',
                questionList: []
            }
        },
        4: {
            serviceContentView: {
                backgroundImage: {
                    1: {
                        webp: require('../assets/webp/ServiceInfo/@1x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@1x.jpeg'),
                    },
                    2: {
                        webp: require('../assets/webp/ServiceInfo/@2x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@2x.jpeg'),
                    },
                    3: {
                        webp: require('../assets/webp/ServiceInfo/@3x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@3x.jpeg'),
                    },
                },
                name: 'UI/UX Design',
                heading: 'UI/UX Design',
                subHeading: '',
                description: 'Rovae, an IT development company in India, provides its clients the best UI/UX experience through its UI/UX design solutions. UI provides a good look to a web or mobile application and UX provides a feel to a web or mobile application. Our UI designers provide a good visual design which includes fonts, color, pagination, sliders and many more elements through UI design and our UX designers provide an easy interactive design which includes easy to use elements like forward and backward button and so on. Our well-experienced UI/UX designers will help you develop the best UI/UX applications who have the experience of 5 years and have served more than 20 happy clients worldwide. Our UI/UX designers are perfect in producing satisfactory results within provided time-limits. So want a good UI/UX design for your mobile-application or website. Then what you are waiting for!',
            },
            questionView: {
                exists: false,
                heading: '',
                subHeading: '',
                questionList: []
            }
        },
        5: {
            serviceContentView: {
                backgroundImage: {
                    1: {
                        webp: require('../assets/webp/ServiceInfo/@1x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@1x.jpeg'),
                    },
                    2: {
                        webp: require('../assets/webp/ServiceInfo/@2x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@2x.jpeg'),
                    },
                    3: {
                        webp: require('../assets/webp/ServiceInfo/@3x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@3x.jpeg'),
                    },
                },
                name: 'Digital Marketing',
                heading: 'Digital Marketing',
                subHeading: '',
                description: 'The power of Internet and its influence on businesses worldwide cannot be ignored. No business, big or small, national or local, can afford to stay away from the online world. Digital marketing service is an evolving field with a lot of techniques and services. Different aspects of this marketing require different skills and expertise. At Rovae, we have an entire digital marketing team to handle all the online marketing needs of your business. Digital marketing is a broad term with a lot of ways and techniques. A rewarding digital marketing strategy needs to have the right balance of all these essential ingredients. Digital marketing is quite different compared to traditional marketing. So, let our experts handle it. Come to us with your marketing objectives and we will be your digital marketing partner!',
            },
            questionView: {
                exists: false,
                heading: '',
                subHeading: '',
                questionList: []
            }
        },
        6: {
            serviceContentView: {
                backgroundImage: {
                    1: {
                        webp: require('../assets/webp/ServiceInfo/@1x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@1x.jpeg'),
                    },
                    2: {
                        webp: require('../assets/webp/ServiceInfo/@2x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@2x.jpeg'),
                    },
                    3: {
                        webp: require('../assets/webp/ServiceInfo/@3x.webp'),
                        jpeg: require('../assets/jpeg/ServiceInfo/@3x.jpeg'),
                    },
                },
                name: 'Mobile Development',
                heading: 'Mobile Development',
                subHeading: '',
                description: 'The procedure for developing the most successful mobile application development service is undertaken here at our company. We try to develop the best mobile application services as per the commands of our client so that their business can take a turn to success. Additionally, our mobile app developers ensure that the full creation of the application is done after the guidance of the clients and their confirmation about certain decisions related to design and structure. To develop user-friendly mobile applications, we take our time in doing research about the industry from where our client is coming. We make sure that our client and we are working towards the same goal and have the same objectives in mind. We are expert in developing user-friendly mobile application solutions for our clients which will help them to optimise their audience and fulfil their organisational goal.',
            },
            questionView: {
                exists: false,
                heading: '',
                subHeading: '',
                questionList: []
            }
        },
    }
}