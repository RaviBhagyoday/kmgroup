import data from './data'

/*
************************************************************************************
    routes: ARRAY(
        OBJECT(
            name: STRING
            [same as what will be shown in RouterView for quick navigation]
            [path separation will happen only for single quoted route name]

            path: STRING

            component: COMPONENT
            props: PROPS
        )
    )
***********************************************************************************
*/
export default {
    routes: [
        /*
        ******************
            Top Level
        ******************
        */
        {
            name: 'Enquiry',
            path: '/enquiry',
            component: () => import('../layouts/Enquiry'),
            props: {
                enquiryView: data.enquiry.enquiryView,
                enquiryTextView: data.enquiry.enquiryTextView
            }
        },
        {
            name: 'Contact',
            path: '/contact',
            component: () => import('../layouts/Contact'),
            props: {
                contactView: data.contact.contactView,
                questionView: data.contact.questionView
            }
        },
        {
            name: 'Tieups',
            path: '/tieups',
            component: () => import('../layouts/Tieups'),
            props: {
                tieupsView: data.tieups.tieupsView
            }
        },
        {
            name: 'Main',
            path: '/',
            component: () => import('../layouts/Main'),
            props: {
                carouselWithBackgroundView: data.main.carouselWithBackgroundView,
                mainCarouselView: data.main.mainCarouselView
            }
        },

        /*
        ******************************
            Products and Services
        ******************************
        */
        {
            name: 'Construction Services',
            path: '/construction+services',
            component: () => import('../layouts/ProductOrService'),
            props: {
                productOrServiceView: data.productOrService[1].productOrServiceView
            }
        },
        {
            name: 'Construction Services / Oil and Gas Products',
            path: '/construction+services/oil+and+gas+products',
            component: () => import('../layouts/ProductList'),
            props: {
                productCardView: data.productList[1].productCardView
            }
        },
        {
            name: 'Construction Services / IT Services',
            path: '/construction+services/it+services',
            component: () => import('../layouts/ServiceList'),
            props: {
                serviceImageView: data.serviceList[1].serviceImageView,
                serviceCardView: data.serviceList[1].serviceCardView,
            }
        },

        /*
        *****************
            Products
        *****************
        */
        {
            name: 'Oil and Gas Products',
            path: '/oil+and+gas+products',
            component: () => import('../layouts/ProductList'),
            props: {
                productCardView: data.productList[1].productCardView
            }
        },
        {
            name: 'Oil and Gas Products / Some Product',
            path: '/oil+and+gas+products/some+product',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView
            }
        },

        {
            name: 'Pencil Products',
            path: '/pencil+products',
            component: () => import('../layouts/ProductList'),
            props: {
                productCardView: data.productList[2].productCardView
            }
        },

        /*
        ******************
            Services
        ******************
        */
        {
            name: 'IT Services',
            path: '/it+services',
            component: () => import('../layouts/ServiceList'),
            props: {
                serviceImageView: data.serviceList[1].serviceImageView,
                serviceCardView: data.serviceList[1].serviceCardView,
            }
        },
        {
            name: 'IT Services / Cloud Solutions',
            path: '/it+services/cloud+solutions',
            component: () => import('../layouts/Service'),
            props: {
                serviceContentView: data.service[1].serviceContentView,
                questionView: data.service[1].questionView
            }
        },
        {
            name: 'IT Services / Web Development',
            path: '/it+services/web+development',
            component: () => import('../layouts/Service'),
            props: {
                serviceContentView: data.service[2].serviceContentView,
                questionView: data.service[2].questionView
            }
        },
        {
            name: 'IT Services / IT Consultancy',
            path: '/it+services/it+consultancy',
            component: () => import('../layouts/Service'),
            props: {
                serviceContentView: data.service[3].serviceContentView,
                questionView: data.service[3].questionView
            }
        },
        {
            name: 'IT Services / UI//UX Design',
            path: '/it+services/ui+uid+design',
            component: () => import('../layouts/Service'),
            props: {
                serviceContentView: data.service[4].serviceContentView,
                questionView: data.service[4].questionView
            }
        },
        {
            name: 'IT Services / Digital Marketing',
            path: '/it+services/digital+marketing',
            component: () => import('../layouts/Service'),
            props: {
                serviceContentView: data.service[5].serviceContentView,
                questionView: data.service[5].questionView
            }
        },
        {
            name: 'IT Services / Mobile Development',
            path: '/it+services/mobile+development',
            component: () => import('../layouts/Service'),
            props: {
                serviceContentView: data.service[6].serviceContentView,
                questionView: data.service[6].questionView
            }
        },
    ]
}