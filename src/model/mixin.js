export default {
    methods: {
      initFadeOut() {
        this.$el.style.opacity = '0.0'

        const observer = new IntersectionObserver((entries, observer) => {
            for(let entry of entries) {
              if(entry.isIntersecting) {
                this.fadeOut(entry.target)
                observer.unobserve(entry.target)
              }
            }
          },
          {
            root: null,
            rootMargin: '0% 20% -20% 20%',
            threshold: 0.35
          }
        )

        observer.observe(this.$el)
      },
      fadeOut(element) {
        let opacity = 0.0
        var interval = setInterval(() => {
          if(opacity > 1.0) clearInterval(interval)
          else {
            element.style.opacity = `${opacity}`
            opacity += 0.1
          }  
        }, 20)
      },
      handleRoute(route) { 
          if(route) {
            if(
                route.path === this.$route.path
                || route.name === this.$route.name
            ) location.reload()
            this.$router.push(route)
          }
      },
      parseSrcset(image) {
        return (
        `${image[1].jpeg} 768w, ${image[2].jpeg} 1024w, ${image[3].jpeg} 1440w, ` +
        `${image[1].webp} 768w, ${image[2].webp} 1024w, ${image[3].webp} 1440w`
        )
      },
      checkEmail(field) {
        let ret = false
        if(field.search(/^.+@.+$/) >= 0) ret = true
        return ret
      },
      checkPhone(field) {
        let ret = true
        if(field.search(/[a-zA-Z]/g) >= 0) ret = false 
        return ret
      },
      pixelToEm(elementSizePixel, rootFontSizePixel = 16, returnUnit = true) {
        let ret = elementSizePixel / rootFontSizePixel
        if(returnUnit) ret += 'em'
        return ret
      },
      emToPixel(elementSizeEm, rootFontSizePixel = 16, returnUnit = true) {
        let ret = elementSizeEm * rootFontSizePixel
        if(returnUnit) ret += 'px'
        return ret
      },
      pixelToVw(elementSizePixel, viewportWidthPixel, returnUnit = true) {
        let ret = 100 / viewportWidthPixel * elementSizePixel
        if(returnUnit) ret += 'vw'
        return ret
      },
      vwToPixel(elementSizeVw, viewportWidthPixel, returnUnit = true) {
        let ret = viewportWidthPixel / 100 * elementSizeVw
        if(returnUnit) ret += 'px'
        return ret
      },
      pixelToPercentage(elementSizePixel, rootFontSizePixel, returnUnit = true) {
        let ret = elementSizePixel / rootFontSizePixel * 100
        if(returnUnit) ret += '%'
        return ret
      },
      percentageToPixel(elementSizePercentage, rootFontSizePixel, returnUnit = true) {
        let ret = elementSizePercentage / 100 * rootFontSizePixel
        if(returnUnit) ret += 'px'
        return ret
      },
    }
}