import data from './data'

/*
************************************************************************************
    routes: ARRAY(
        OBJECT(
            name: STRING
            [same as what will be shown in RouterView for quick navigation]
            [path separation will happen only for single quoted route name]

            path: STRING

            component: COMPONENT
            props: PROPS
        )
    )
***********************************************************************************
*/
export default {
    routes: [
        /*
        ******************
            Top Level
        ******************
        */
        {
            name: 'Enquiry',
            path: '/enquiry',
            component: () => import('../layouts/Enquiry'),
            props: {
                enquiryView: data.enquiry.enquiryView,
                enquiryTextView: data.enquiry.enquiryTextView
            }
        },
        {
            name: 'Contact',
            path: '/contact',
            component: () => import('../layouts/Contact'),
            props: {
                contactView: data.contact.contactView,
                questionView: data.contact.questionView
            }
        },
        // {
        //     name: 'Tieups',
        //     path: '/tieups',
        //     component: () => import('../layouts/Tieups'),
        //     props: {
        //         tieupsView: data.tieups.tieupsView
        //     }
        // },
        {
            name: 'Main',
            path: '/',
            component: () => import('../layouts/Main'),
            props: {
                carouselWithBackgroundView: data.main.carouselWithBackgroundView,
                mainCarouselView: data.main.mainCarouselView
            }
        },

        /*
        ******************************
            Products and Services
        ******************************
        */
        {
            name: 'Construction Services',
            path: '/construction+services',
            component: () => import('../layouts/ProductOrService'),
            props: {
                productOrServiceView: data.productOrService[1].productOrServiceView
            }
        },
        {
            name: 'Construction Services / Oil and Gas Products',
            path: '/construction+services/oil+and+gas+products',
            component: () => import('../layouts/ProductList'),
            props: {
                productCardView: data.productList[1].productCardView
            }
        },
        {
            name: 'Construction Services / IT Services',
            path: '/construction+services/it+services',
            component: () => import('../layouts/ServiceList'),
            props: {
                serviceImageView: data.serviceList[1].serviceImageView,
                serviceCardView: data.serviceList[1].serviceCardView,
            }
        },

        /*
        *****************
            Products
        *****************
        */
        {
            name: 'Industrial Products',
            path: '/industrial+products',
            component: () => import('../layouts/ProductList'),
            props: {
                productCardView: data.productList[1].productCardView
            }
        },
                /*
        *****************
            INDUSTRIAL PRODUCTS
        *****************
        */
        {
            name: 'Industrial Products / UPS',
            path: '/industrial+products/ups',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 0

            }
        },
        {
            name: 'Industrial Products / Sleeving',
            path: '/industrial+products/sleeving',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 1
            }
        },
        {
            name: 'Industrial Products / Valves',
            path: '/industrial+products/valves',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 2
            }
        },
        {
            name: 'Industrial Products / Transformers',
            path: '/industrial+products/transformers',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 3
            }
        },
        {
            name: 'Industrial Products / Encoders & Sensors',
            path: '/industrial+products/sensors',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 4
            }
        },
        {
            name: 'Industrial Products / Relays',
            path: '/industrial+products/relays',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 5
            }
        },
        {
            name: 'Industrial Products / Pumps',
            path: '/industrial+products/pumps',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 6
            }
        },
        {
            name: 'Industrial Products / Motors',
            path: '/industrial+products/motors',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 7
            }
        },
        {
            name: 'Industrial Products / Pressure & Temperature',
            path: '/industrial+products/pandt',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 8
            }
        },
        {
            name: 'Industrial Products / Testers',
            path: '/industrial+products/testers',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 9
            }
        },
        {
            name: 'Industrial Products / Cables',
            path: '/industrial+products/cables',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 10
            }
        },
        {
            name: 'Industrial Products / Electronics',
            path: '/industrial+products/electronics',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 11
            }
        },
        {
            name: 'Industrial Products / Instruments',
            path: '/industrial+products/instruments',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 12
            }
        },
        {
            name: 'Industrial Products / Bearing',
            path: '/industrial+products/bearing',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 13
            }
        },
        {
            name: 'Industrial Products / Battery',
            path: '/industrial+products/battery',
            component: () => import('../layouts/Product'),
            props: {
                productInfoView: data.product[1].productInfoView,
                start: 14
            }
        },

        /*
        ******************
            Services
        ******************
        */
        {
            name: 'IT Services',
            path: '/it+services',
            component: () => import('../layouts/ServiceList'),
            props: {
                serviceImageView: data.serviceList[1].serviceImageView,
                serviceCardView: data.serviceList[1].serviceCardView,
            }
        },
        {
            name: 'IT Services / Cloud Solutions',
            path: '/it+services/cloud+solutions',
            component: () => import('../layouts/Service'),
            props: {
                serviceContentView: data.service[1].serviceContentView,
                questionView: data.service[1].questionView
            }
        },
        {
            name: 'IT Services / Web Development',
            path: '/it+services/web+development',
            component: () => import('../layouts/Service'),
            props: {
                serviceContentView: data.service[2].serviceContentView,
                questionView: data.service[2].questionView
            }
        },
        {
            name: 'IT Services / IT Consultancy',
            path: '/it+services/it+consultancy',
            component: () => import('../layouts/Service'),
            props: {
                serviceContentView: data.service[3].serviceContentView,
                questionView: data.service[3].questionView
            }
        },
        {
            name: 'IT Services / UI//UX Design',
            path: '/it+services/ui+uid+design',
            component: () => import('../layouts/Service'),
            props: {
                serviceContentView: data.service[4].serviceContentView,
                questionView: data.service[4].questionView
            }
        },
        {
            name: 'IT Services / Digital Marketing',
            path: '/it+services/digital+marketing',
            component: () => import('../layouts/Service'),
            props: {
                serviceContentView: data.service[5].serviceContentView,
                questionView: data.service[5].questionView
            }
        },
        {
            name: 'IT Services / Mobile Development',
            path: '/it+services/mobile+development',
            component: () => import('../layouts/Service'),
            props: {
                serviceContentView: data.service[6].serviceContentView,
                questionView: data.service[6].questionView
            }
        },
    ]
}