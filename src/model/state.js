export default {
    state: {
        notification: '',
        navSidebarOpen: false,
        layoutColor: 'transparent',
        routeShow: false,
        colors: {
            colorBackground: '#F4F4F4',
            colorBackgroundDark: '#EDEDED',

            colorPrimary: '#FFFFFF',
            colorPrimaryDark: '#091C27',
            colorSecondaryDark: '#000000',

            colorDisabled: '#E5E5E5',

            colorGrey: '#4E616C',
            colorGreyLight: '#697983',
            colorGreyImgBack: '#8C8C8C',
            colorGreyLightDimmed: '#CECECE',
            colorGreyDarkDimmed: '#F4F4F4',

            colorPink: '#E28C93',

            colorAccent: '#DF0818',
            colorAccentLight: '#C7BABA',
            colorAccentFocused: '#091AAC',
            colorAccentLightDimmed: '#FFFAFA',
            
            colorTextDark: '#494949',
            colorTextLight: '#FFFFFF',
            colorTextLightDimmed: '#A6A5A5',
            colorTextUnfocused: '#767676',
            colorTextDisabled: '#CACACA',
            colorTextDarkDimmed: '#959494',
            colorTextDarkDarkDimmed: '#1A1A1A'
        }
    },
    mutations: {
        setNavSidebarOpen(state, value) {
            state.navSidebarOpen = value
        },
        setLayoutColor(state, color) {
            state.layoutColor = color
        },
        setNotification(state, notification) {
            state.notification = notification
        }
    },
    getters: {
        navSidebarOpen(state) {
            return state.navSidebarOpen
        },
        colors(state) {
            return state.colors
        },
        layoutColor(state) {
            return state.layoutColor
        },
        notification(state) {
            return state.notification
        }
    }
}