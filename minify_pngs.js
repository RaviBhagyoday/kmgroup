const sharp = require('sharp')
const fs = require('fs').promises
const ncp = require('ncp').ncp
const rimraf = require('rimraf')

ncp.limit = 3

const SRC = './png'
const JPEG = './src/assets/jpeg'
const WEBP = './src/assets/webp'

function copyJPEG() {
    return new Promise((resolve, reject) => {
        fs.stat(JPEG)
        .then(() => { 
            rimraf(JPEG, (err) => {
                if(err) console.log(`copyJPEG :: fs.stat :: success :: rimraf :: error`)
                else ncp(SRC, JPEG, (err) => {
                    if(err) reject(`copyJPEG :: fs.stat :: success :: rimraf :: ncp :: error :: ${err}`)
                    else resolve(`copyJPEG :: fs.stat :: success :: rimraf :: ncp :: success`)
                })
            })
        })
        .catch((err) => { 
            if(err.code === 'ENOENT') {
                ncp(SRC, JPEG, (err) => {
                    if(err) reject(`copyJPEG :: fs.stat :: error :: ncp :: error :: ${err}`)
                    else resolve(`copyJPEG :: fs.stat :: error :: ncp :: success`)
                })
            } else {
                reject(`copyJPEG :: fs.stat :: error :: ${err}`)
            }
        })
    })
}

function copyWEBP() {
    return new Promise((resolve, reject) => {
        fs.stat(WEBP)
        .then(() => { 
            rimraf(WEBP, (err) => {
                if(err) console.log(`copyWEBP :: fs.stat :: success :: rimraf :: error`)
                else ncp(SRC, JPEG, (err) => {
                    if(err) reject(`copyWEBP :: fs.stat :: success :: rimraf :: ncp :: error :: ${err}`)
                    else resolve(`copyWEBP :: fs.stat :: success :: rimraf :: ncp :: success`)
                })
            })
        })
        .catch((err) => { 
            if(err.code === 'ENOENT') {
                ncp(SRC, WEBP, (err) => {
                    if(err) reject(`copyWEBP :: fs.stat :: error :: ncp :: error :: ${err}`)
                    else resolve(`copyWEBP :: fs.stat :: error :: ncp :: success`)
                })
            } else {
                reject(`copyWEBP :: fs.stat :: error :: ${err}`)
            }
        })
    })
}

function mini(height, width, reduction) {
    return { 
        height: parseInt(Math.ceil(height/reduction)), 
        width: parseInt(Math.ceil(width/reduction)), 
        fit: 'outside' 
    }
}

async function processJPEG() {
    new Promise((resolve, reject) => {
        fs.readdir(JPEG)
        .then(async () => {
            const dirList = await fs.readdir(JPEG)
            dirList.forEach(async (dir) => {
                try{ 
                    console.log(`Processing :: jpeg / ${dir}`)
                    const img = sharp(`${JPEG}/${dir}/img.png`)

                    const meta = await img.metadata()
                    const H = meta.height 
                    const W = meta.width

                    await img.resize(mini(H, W, 3.5)).jpeg({ quality: 60 }).toFile(`${JPEG}/${dir}/@1x.jpeg`)
                    await img.resize(mini(H, W, 3.0)).jpeg({ quality: 75 }).toFile(`${JPEG}/${dir}/@2x.jpeg`)
                    await img.resize(mini(H, W, 2.5)).jpeg({ quality: 80 }).toFile(`${JPEG}/${dir}/@3x.jpeg`)

                    await fs.unlink(`${JPEG}/${dir}/img.png`)

                    resolve(`processJPEG :: fs.readdir :: success :: dirList :: ${dir} :: success`)
                } catch(e) {
                    reject(`processJPEG :: fs.readdir :: success :: dirList :: ${dir} :: ${e}`)
                }
            })
        })
        .catch((err) => {
            reject(`processJPEG :: fs.readdir :: error :: ${err}`)
        })
    })
}

async function processWEBP() {
    new Promise((resolve, reject) => {
        fs.readdir(WEBP)
        .then(async () => {
            const dirList = await fs.readdir(WEBP)
            dirList.forEach(async (dir) => {
                try{ 
                    console.log(`Processing :: webp / ${dir}`)
                    const img = sharp(`${WEBP}/${dir}/img.png`)

                    const meta = await img.metadata()
                    const H = meta.height 
                    const W = meta.width

                    await img.resize(mini(H, W, 3.5)).webp({ quality: 20 }).toFile(`${WEBP}/${dir}/@1x.webp`)
                    await img.resize(mini(H, W, 3.0)).webp({ quality: 30 }).toFile(`${WEBP}/${dir}/@2x.webp`)
                    await img.resize(mini(H, W, 2.5)).webp({ quality: 40 }).toFile(`${WEBP}/${dir}/@3x.webp`)

                    await fs.unlink(`${WEBP}/${dir}/img.png`)

                    resolve(`processWEBP :: fs.readdir :: success :: dirList :: ${dir} :: success`)
                } catch(e) {
                    reject(`processWEBP :: fs.readdir :: success :: dirList :: ${dir} :: ${e}`)
                }
            })
        })
        .catch((err) => {
            reject(`processWEBP :: fs.readdir :: error :: ${err}`)
        })
    })
}

async function minify() {
    try {
        await copyJPEG()
        await copyWEBP()
        
        await processJPEG()
        await processWEBP()
    } catch(e) {
        console.log(`minify :: ${e}`)
    }
}
minify()
.then(() => { console.log('TERMINATED') })
.catch(() => { console.log('ERROR') })