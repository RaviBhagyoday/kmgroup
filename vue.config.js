const config = {}

config.css = {
    loaderOptions: {
        sass: {
            additionalData: `@import "@/css/index.scss";`
        }
    }
}

module.exports = config